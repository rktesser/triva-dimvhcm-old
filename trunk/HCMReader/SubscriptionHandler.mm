#import "SubscriptionHandler.h"
#import "SubscriptionManager.h"

SubscriptionManager *subscrManager;

@implementation SubscriptionHandler

/*****************************************************************************
 * Implementation of the HCMSubscriptionHandler protocol.                    *
 ****************************************************************************/

- (SubscriptionHandler *)init
{
	self = [super init];
	subscrManager = new SubscriptionManager((wxWindow *)NULL);
	subscrManager->setSubscriptionHandler(self);
	subscrManager->Show();
	return self;
}

- (BOOL)registerCollectorWithId: (NSString *)collectorId
  type: (NSString *)type location: (NSString *)location
{
	subscrManager->addCollector(collectorId, type, location);
//	[self subscribeToCollector: collectorId];
	return YES;
}

- (BOOL)unregisterCollectorWithId: (NSString *)collectorId
{
	subscrManager->removeCollector(collectorId);
	return YES;
}

- (void)setClient: (DIMVClient *)aClient
{
	HCMClient = aClient;
	return;
}

/*****************************************************************************/

/*****************************************************************************
 * Methods used by the SubscriptionManager object.                           * 
 *****************************************************************************/

- (BOOL)subscribeToCollector: (NSString *)collectorId
{
	BOOL ret;
	[collectorId retain];
	ret = [HCMClient subscribeToCollector: collectorId];
	[collectorId release];
	return ret;
}

- (BOOL)unsubscribeToCollector: (NSString *)collectorId
{
	BOOL ret;
	[collectorId retain];
	ret = [HCMClient unsubscribeToCollector: collectorId];
	[collectorId release];
	return ret;
}

/*****************************************************************************/

@end
