///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Dec 29 2008)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#ifndef __SubscriptionManagerFrame__
#define __SubscriptionManagerFrame__

#include <wx/string.h>
#include <wx/radiobox.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/listbox.h>
#include <wx/checklst.h>
#include <wx/sizer.h>
#include <wx/frame.h>

///////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
/// Class SubscriptionManagerFrame
///////////////////////////////////////////////////////////////////////////////
class SubscriptionManagerFrame : public wxFrame 
{
	private:
	
	protected:
		wxRadioBox* groupingSelRadioBox1;
		wxListBox* groupsList;
		wxCheckListBox* collectorsList;
		
		// Virtual event handlers, overide them in your derived class
		virtual void onGroupingTypeSelection( wxCommandEvent& event ){ event.Skip(); }
		virtual void onGroupSelection( wxCommandEvent& event ){ event.Skip(); }
		virtual void onCollectorToggle( wxCommandEvent& event ){ event.Skip(); }
		
	
	public:
		SubscriptionManagerFrame( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Subscription Manager"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 500,497 ), long style = wxDEFAULT_FRAME_STYLE|wxTAB_TRAVERSAL );
		~SubscriptionManagerFrame();
	
};

#endif //__SubscriptionManagerFrame__
