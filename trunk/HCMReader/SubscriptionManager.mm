#include "SubscriptionManager.h"


SubscriptionManager::SubscriptionManager( wxWindow* parent )
:
SubscriptionManagerFrame( parent )
{
	collectorGrouping = _HR_GROUP_BY_TYPE_;
	collectorsById = [[NSMutableDictionary alloc] init];
	collectorsByType = [[NSMutableDictionary alloc] init];
	collectorsByAddress = [[NSMutableDictionary alloc] init];
	collectorSelection = [[NSMutableDictionary alloc] init];
	typeList = wxArrayString::wxArrayString();
	addressList = wxArrayString::wxArrayString();
}

void SubscriptionManager::onGroupingTypeSelection( wxCommandEvent& event )
{
	if(event.GetString().IsSameAs(wxT("by Type"))){
		collectorGrouping = _HR_GROUP_BY_TYPE_;
		groupsList->Set(typeList);
	}else if(event.GetString().IsSameAs(wxT("by Address"))){
		collectorGrouping = _HR_GROUP_BY_ADDRESS_;
		groupsList->Set(addressList);
	}
}

void SubscriptionManager::onGroupSelection( wxCommandEvent& event )
{
	NSMutableDictionary *collectors;
	NSEnumerator *collectorEnum;
	NSString *collectorType, *collectorAddress, *collectorId;
	NSString *itemText;
	wxArrayInt selections;
	int i, n, pos;
	n = groupsList->GetSelections(selections);
//	wxArrayString items = wxArrayString::wxArrayString();
	collectorsList->Clear();
	for(i = 0; i < n; i++){
		NSString *key = [NSString stringWithUTF8String:
		  (groupsList->GetString(selections.Item(i)).mb_str())];
		[key retain];
		CollectorRegister *colReg;
		if(collectorGrouping == _HR_GROUP_BY_TYPE_){
			collectors = [collectorsByType objectForKey: key];
			[key release];
			collectorEnum = [collectors objectEnumerator];
			while((colReg = [collectorEnum nextObject]) != nil){
				collectorType = [colReg getCollectorType];
				collectorAddress = [colReg
				  getCollectorLocation];
				collectorId = [colReg getCollectorId];
				itemText = [NSString stringWithFormat: 
				  @"%@ - %@ (%@)", collectorType,
				  collectorAddress, collectorId];
/*				items.Add(wxString::FromUTF8([
				  itemText UTF8String]));*/
				pos = collectorsList->Append(
				  wxString::FromUTF8([itemText UTF8String]), 
				  colReg);
				if([[collectorSelection objectForKey:
				  collectorId] isEqualToString: @"YES"] == YES){
				 	collectorsList->Check(pos);
				}
			}
		}else{
			collectors = [collectorsByAddress objectForKey: key];
			[key release];
			collectorEnum = [collectors objectEnumerator];
			while((colReg = [collectorEnum nextObject]) != nil){
				collectorType = [colReg getCollectorType];
				collectorAddress = [colReg
				  getCollectorLocation];
				collectorId = [colReg getCollectorId];
				itemText = [NSString stringWithFormat:
				  @"%@ - %@ (%@)", collectorAddress, 
				  collectorType, collectorId];
/*				items.Add(wxString::FromUTF8([
				  itemText UTF8String]));*/
				pos = collectorsList->Append(
				  wxString::FromUTF8([  itemText UTF8String]),
				  colReg);
				if([[collectorSelection objectForKey:
				  collectorId] isEqualToString: @"YES"] == YES){
				 	collectorsList->Check(pos);
				}
			}
		}
	}
//	collectorsList->Set(items);
}

void SubscriptionManager::onCollectorToggle( wxCommandEvent& event )
{
	NSString *collectorId;
	CollectorRegister *colReg;
/*	wxString desc = collectorsList->GetString(event.GetInt());
	NSString *key = [NSString stringWithUTF8String: desc.mb_str()];
	[key retain];
	NSLog(@"KEY: %@", key);
	if(collectorGrouping == _HR_GROUP_BY_TYPE_){
		colReg = [collectorsByType objectForKey:key];
	}else if(collectorGrouping == _HR_GROUP_BY_ADDRESS_){
		colReg = [collectorsByAddress objectForKey:key];
	}
	[key release];*/
	colReg = (CollectorRegister *)collectorsList->GetClientData(
	  event.GetInt());
	collectorId = [colReg getCollectorId];
	if(collectorsList->IsChecked(event.GetInt())){
		NSLog(@"Subscribing to: %@", collectorId);
		[collectorSelection setObject: @"YES" forKey: collectorId];
		[SMSubscrHandler subscribeToCollector: collectorId];
	}else{
		NSLog(@"Unsubscribing to: %@", collectorId);
		[collectorSelection setObject: @"NO" forKey: collectorId];
		[SMSubscrHandler unsubscribeToCollector: collectorId];
	}
}

void SubscriptionManager::setSubscriptionHandler( SubscriptionHandler *aHandler )
{
	SMSubscrHandler = aHandler;
}

void SubscriptionManager::addCollector(NSString *collectorId, NSString *type, NSString *location)
{
	wxString groupName, itemText;
	int i, n, pos;
	wxArrayInt selectedGroups;
	NSMutableDictionary *collectorsInGroup;
	CollectorRegister *colReg = [[CollectorRegister alloc] 
	  initWithCollectorId: collectorId andType: type 
	  andLocation: location];
	if((collectorsInGroup = [collectorsByType objectForKey: type]) == nil){
		collectorsInGroup = [[NSMutableDictionary alloc] init];
		[collectorsByType setObject: collectorsInGroup forKey: type];
		[collectorsInGroup release];
		typeList.Add(wxString::FromUTF8([type UTF8String]));
		if(collectorGrouping == _HR_GROUP_BY_TYPE_){
//			groupsList->Set(typeList);
			groupsList->Append(wxString::FromUTF8(
			  [type UTF8String]));
		}
	}
	[collectorsInGroup setObject: colReg forKey: collectorId];

	if((collectorsInGroup = [collectorsByAddress objectForKey: 
	  location]) == nil){
		collectorsInGroup = [[NSMutableDictionary alloc] init];
		[collectorsByAddress setObject: collectorsInGroup forKey: location];
		[collectorsInGroup release];
		addressList.Add(wxString::FromUTF8([location UTF8String]));
		if( collectorGrouping == _HR_GROUP_BY_ADDRESS_){
//			groupsList->Set(addressList);
			groupsList->Append(wxString::FromUTF8(
			  [location UTF8String]));
		}
	}
	[collectorsInGroup setObject: colReg forKey: collectorId];

	[collectorSelection setObject: @"NO" forKey: collectorId];
	

	if(collectorGrouping == _HR_GROUP_BY_TYPE_){
		groupName = wxString::FromUTF8([type UTF8String]);
		itemText = wxString::FromUTF8([[NSString stringWithFormat: 
		  @"%@ - %@ (%@)", type, location, collectorId] UTF8String]);
	}else{
		groupName = wxString::FromUTF8([location UTF8String]);
		itemText = wxString::FromUTF8([[NSString stringWithFormat: 
		  @"%@ - %@ (%@)", location, type, collectorId] UTF8String]);
	}
	n = groupsList->GetSelections(selectedGroups);
	for(i = 0; i < n; i++){
		NSLog(@"%d", i);
		pos = selectedGroups.Item(i);
		if(groupName.IsSameAs(groupsList->GetString(pos),true)){
			collectorsList->Append(itemText, colReg);
		}
	}
	[collectorsById setObject: colReg forKey: collectorId];
	[colReg release];
	
	return;
}

void SubscriptionManager::removeCollector(NSString *collectorId)
{
	int pos;
	NSString *location,*type;
	wxString groupName;
	NSString *itemText;
	CollectorRegister *colReg = [collectorsById objectForKey: collectorId];
	[colReg retain];
	location = [colReg getCollectorLocation];
	type = [colReg getCollectorType];
	[collectorsById removeObjectForKey: collectorId];
	[[collectorsByType objectForKey: type] removeObjectForKey: 
	  collectorId];
	[[collectorsByAddress objectForKey: location] removeObjectForKey:
	  collectorId];
	[collectorSelection removeObjectForKey: collectorId];
	if(collectorGrouping == _HR_GROUP_BY_TYPE_){
		groupName = wxString::FromUTF8([type UTF8String]);
		itemText = [NSString stringWithFormat: 
		  @"%@ - %@ (%@)", type, location, 
		  collectorId];
	}else{ // colllectorGrouping == _HR_GROUP_BY_ADDRESS_
		groupName = wxString::FromUTF8([location UTF8String]);
		itemText = [NSString stringWithFormat: 
		  @"%@ - %@ (%@)", location, type, 
		  collectorId];
	}
	pos = groupsList->FindString(groupName, true);
	if( groupsList->IsSelected(pos)){
		pos = collectorsList->FindString(
		  wxString::FromUTF8([itemText UTF8String]), true);
		collectorsList->Delete(pos);
	}
	[colReg release];
}
