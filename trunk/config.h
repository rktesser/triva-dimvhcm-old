/* config.h.  Generated from config.h.in by configure.  */
/* config.h.in.  Generated from configure.ac by autoheader.  */

/* Define to 1 if you have the `bzero' function. */
#define HAVE_BZERO 1

/* Define if CommunicationPattern visualization components is activated */
/* #undef HAVE_COMMUNICATIONPATTERN */

/* Define if Dot output is activated */
/* #undef HAVE_DOT */

/* Define to 1 if you have the <float.h> header file. */
#define HAVE_FLOAT_H 1

/* Enable GraphConfiguration */
/* #undef HAVE_GRAPHCONFIGURATION */

/* Define if HCMReader visualization components is activated */
#define HAVE_HCMREADER 1

/* Define to 1 if you have the <inttypes.h> header file. */
#define HAVE_INTTYPES_H 1

/* Define to 1 if you have the `gvc' library (-lgvc). */
/* #undef HAVE_LIBGVC */

/* Define to 1 if you have the <limits.h> header file. */
#define HAVE_LIMITS_H 1

/* Define to 1 if you have the <memory.h> header file. */
#define HAVE_MEMORY_H 1

/* Define if NetworkTopology visualization components is activated */
/* #undef HAVE_NETWORKTOPOLOGY */

/* Define if NUCAView visualization components is activated */
/* #undef HAVE_NUCAVIEW */

/* Define if SimGrid visualization components is activated */
/* #undef HAVE_SIMGRID */

/* Define if SquarifiedTreemap visualization components is activated */
/* #undef HAVE_SQUARIFIEDTREEMAP */

/* Define to 1 if stdbool.h conforms to C99. */
#define HAVE_STDBOOL_H 1

/* Define to 1 if you have the <stdint.h> header file. */
#define HAVE_STDINT_H 1

/* Define to 1 if you have the <stdlib.h> header file. */
#define HAVE_STDLIB_H 1

/* Define to 1 if you have the <strings.h> header file. */
#define HAVE_STRINGS_H 1

/* Define to 1 if you have the <string.h> header file. */
#define HAVE_STRING_H 1

/* Define if the subscription management interface of the HCMReader
   visualization components is activated. */
/* #undef HAVE_SUBSCRIPTIONMANAGER */

/* Define to 1 if you have the <sys/stat.h> header file. */
#define HAVE_SYS_STAT_H 1

/* Define to 1 if you have the <sys/time.h> header file. */
#define HAVE_SYS_TIME_H 1

/* Define to 1 if you have the <sys/types.h> header file. */
#define HAVE_SYS_TYPES_H 1

/* Define to 1 if you have the <unistd.h> header file. */
#define HAVE_UNISTD_H 1

/* Define to 1 if the system has the type `_Bool'. */
/* #undef HAVE__BOOL */

/* Name of package */
#define PACKAGE "Triva"

/* Define to the address where bug reports for this package should be sent. */
#define PACKAGE_BUGREPORT "schnorr@gmail.com"

/* Define to the full name of this package. */
#define PACKAGE_NAME "Triva"

/* Define to the full name and version of this package. */
#define PACKAGE_STRING "Triva 2.0"

/* Define to the one symbol short name of this package. */
#define PACKAGE_TARNAME "Triva"

/* Define to the home page for this package. */
#define PACKAGE_URL ""

/* Define to the version of this package. */
#define PACKAGE_VERSION "2.0"

/* Define to 1 if you have the ANSI C header files. */
#define STDC_HEADERS 1

/* Version number of package */
#define VERSION "2.0"
