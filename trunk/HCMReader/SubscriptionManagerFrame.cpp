///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Dec 29 2008)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "SubscriptionManagerFrame.h"

///////////////////////////////////////////////////////////////////////////

SubscriptionManagerFrame::SubscriptionManagerFrame( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxFrame( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	
	wxBoxSizer* bSizer4;
	bSizer4 = new wxBoxSizer( wxVERTICAL );
	
	wxString groupingSelRadioBox1Choices[] = { wxT("by Type"), wxT("by Address") };
	int groupingSelRadioBox1NChoices = sizeof( groupingSelRadioBox1Choices ) / sizeof( wxString );
	groupingSelRadioBox1 = new wxRadioBox( this, wxID_ANY, wxT("Group Collectors"), wxDefaultPosition, wxDefaultSize, groupingSelRadioBox1NChoices, groupingSelRadioBox1Choices, 1, wxRA_SPECIFY_ROWS );
	groupingSelRadioBox1->SetSelection( 0 );
	bSizer4->Add( groupingSelRadioBox1, 0, wxALIGN_CENTER_HORIZONTAL|wxALL, 5 );
	
	wxGridSizer* gSizer1;
	gSizer1 = new wxGridSizer( 1, 2, 0, 0 );
	
	groupsList = new wxListBox( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0, NULL, wxLB_EXTENDED|wxLB_SORT ); 
	gSizer1->Add( groupsList, 0, wxALL|wxEXPAND, 5 );
	
	wxArrayString collectorsListChoices;
	collectorsList = new wxCheckListBox( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, collectorsListChoices, 0 );
	gSizer1->Add( collectorsList, 0, wxALL|wxEXPAND, 5 );
	
	bSizer4->Add( gSizer1, 1, wxEXPAND, 5 );
	
	this->SetSizer( bSizer4 );
	this->Layout();
	
	// Connect Events
	groupingSelRadioBox1->Connect( wxEVT_COMMAND_RADIOBOX_SELECTED, wxCommandEventHandler( SubscriptionManagerFrame::onGroupingTypeSelection ), NULL, this );
	groupsList->Connect( wxEVT_COMMAND_LISTBOX_SELECTED, wxCommandEventHandler( SubscriptionManagerFrame::onGroupSelection ), NULL, this );
	collectorsList->Connect( wxEVT_COMMAND_CHECKLISTBOX_TOGGLED, wxCommandEventHandler( SubscriptionManagerFrame::onCollectorToggle ), NULL, this );
}

SubscriptionManagerFrame::~SubscriptionManagerFrame()
{
	// Disconnect Events
	groupingSelRadioBox1->Disconnect( wxEVT_COMMAND_RADIOBOX_SELECTED, wxCommandEventHandler( SubscriptionManagerFrame::onGroupingTypeSelection ), NULL, this );
	groupsList->Disconnect( wxEVT_COMMAND_LISTBOX_SELECTED, wxCommandEventHandler( SubscriptionManagerFrame::onGroupSelection ), NULL, this );
	collectorsList->Disconnect( wxEVT_COMMAND_CHECKLISTBOX_TOGGLED, wxCommandEventHandler( SubscriptionManagerFrame::onCollectorToggle ), NULL, this );
}
