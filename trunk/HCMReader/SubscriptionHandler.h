#ifndef __TRIVA_SUBSCRIPTIONHANDLER__
#define __TRIVA_SUBSCRIPTIONHANDLER__

#import <Foundation/Foundation.h>
#import <DIMV-HCM/DIMVClientProto.h>
#import <DIMV-HCM/DIMVClient.h>

@interface SubscriptionHandler : NSObject <HCMSubscriptionHandler>
{
	DIMVClient *HCMClient;
}

/*****************************************************************************
 * Methods from the HCMSubscriptionHandler protocol.                         *
 *****************************************************************************/

- (BOOL)registerCollectorWithId: (NSString *)collectorId
  type: (NSString *)type location: (NSString *)location;
- (BOOL)unregisterCollectorWithId: (NSString *)collectorId;
- (void)setClient: (DIMVClient *)client;

/*****************************************************************************/

/*****************************************************************************
 * Methods used by the SubscriptionManager object.                           * 
 *****************************************************************************/


- (BOOL)subscribeToCollector: (NSString *)collectorId;
- (BOOL)unsubscribeToCollector: (NSString *)collectorId;

/*****************************************************************************/

@end

#endif

