#include "DrawManager.h"
#include "Position.h"

Ogre::SceneNode *DrawManager::drawOneContainer (id cont, Ogre::SceneNode *node,
		float x, float y)
{
        std::string orname = std::string ([[cont name] cString]);
        std::string name = std::string(orname);
        name.append ("-#-#-");
        name.append ([[[cont entityType] name] cString]);

	/* creating or re-using the container scene node */
        Ogre::SceneNode *n;
        try {
                n = mSceneMgr->getSceneNode (orname);
        	n->setPosition (x, 0, y);
                return n;
        }catch (Ogre::Exception ex){
                n = node->createChildSceneNode (orname);
        }
        n->setPosition(x, 0, y);

	/* creating or re-using the visual representation of the container */
        Ogre::Entity *e;
        try {
                e = mSceneMgr->getEntity(orname);
        }catch (Ogre::Exception ex){
                e = mSceneMgr->createEntity (orname,
                                Ogre::SceneManager::PT_CUBE);
        }
        e->setUserAny (Ogre::Any (name));
        e->setMaterialName ("VisuApp/Base");
        e->setQueryFlags(CONTAINER_MASK);

	/* creating or re-using the visual scene node of the container */
	std::string visualSceneNodeName = std::string (orname);
	visualSceneNodeName.append("visualRepresentation");
	Ogre::SceneNode *entn;
	try {
		entn = mSceneMgr->getSceneNode (visualSceneNodeName);
	}catch (Ogre::Exception ex){
		entn = n->createChildSceneNode(visualSceneNodeName);
//		entn->attachObject (e);
		entn->setScale (.3,.01,.3);
		entn->setInheritScale (false);
	}

	/* creating or re-using the text scene node of the container */
	std::string textSceneNodeName = std::string (orname);
	textSceneNodeName.append("textRepresentation");
	Ogre::SceneNode *entnt;
	try{	
                entnt = mSceneMgr->getSceneNode (textSceneNodeName);
        }catch(Ogre::Exception ex){
                entnt = n->createChildSceneNode (textSceneNodeName);
                MovableText *text;
                NSString *textid;
                textid = [NSString stringWithFormat: @"%@-t", [cont name]];
                text = new MovableText ([textid cString], [textid cString]);
                text->setColor (Ogre::ColourValue::Blue);
                text->setCharacterHeight (15);
                entnt->setInheritScale (false);
//                entnt->attachObject (text);
        }
	return n;
}
